function RandomFactsController(model, view) {

  var self = this;

  /**
   * INIT
   */

  //this.init = function() { setTouch(); dataFetch("data/facts.json", setLocalData); }
  this.init = function() { setTouch(); dataFetch(googleQuestion, setGoogleQuestions); }

  /**
   * TOUCH AND SHAKE
   */

  var shakeTimeout;
  
  var shake = function() {
    if (shakeTimeout) { clearTimeout(shakeTimeout); }
    shakeTimeout = setTimeout(shakeView, 250);
  }

  var shakeView = function() {
    if (model.getViewSelected() == "quiz") {
      model.setQuizShuffleSelected();
      // update view
      viewSelect("quiz");
    }
  }

  var setTouch = function() {
    // prevent swipe
    $("body").on("touchmove", function(e) { e.preventDefault(); });

    // set shake
    if (typeof window.DeviceMotionEvent != "undefined") {
      // shake sensitivity (lower number more sensitive)
      var sensitivity = 20;
      // position variables
      var x1 = 0, y1 = 0, z1 = 0, x2 = 0, y2 = 0, z2 = 0;
      // motion events listener updates position
      window.addEventListener("devicemotion", function (e) {
        x1 = e.accelerationIncludingGravity.x;
        y1 = e.accelerationIncludingGravity.y;
        z1 = e.accelerationIncludingGravity.z;
      }, false);
      // fire if change greater than sensitivity
      setInterval(function () {
        var change = Math.abs(x1-x2+y1-y2+z1-z2);
        if (change > sensitivity) { shake(); }
        // update position
        x2 = x1; y2 = y1; z2 = z1;
      }, 150);
    }
  }

  /**
   * DATA LOADER
   */

  var googleQuestion = "https://spreadsheets.google.com/feeds/list/17j7Wnd0Ncq6mmZzBK5ek-wJMRh91iFsGf_W1yD4x6Do/od6/public/values?alt=json";
  var googleExtraText = "https://spreadsheets.google.com/feeds/list/1yvWSFf97ddK9n-V3_TjQHGgVaAfdvD79r5a2FoTJI-w/od6/public/values?alt=json";

  var dataFetch = function(url, callback) {
    $.ajax({ url: url, type: "get", dataType: "json",
      success: function(data) { callback(data); } });
  }
  
  var setLocalData = function(data) {
    model.setQuizLocalData(data);
    model.getCookie();
    // update view
    viewSelect();
  }

  var setGoogleQuestions = function(data) {
    model.setGoogleQuestions(data);
    dataFetch(googleExtraText, setGoogleExtraText);
  }

  var setGoogleExtraText = function(data) {
    model.setGoogleExtraText(data);
    model.getCookie();
    // update view
    viewSelect();
  }

  /**
   * RESET
   */
  
  var resetSelect = function() {
    // remove cookie
    model.removeCookie();
    // reset
    model.resetQuizData();
    model.setScore();
    // update view
    viewSelect("quiz");
  }

  /**
   * VIEW
   */
  
  var viewSelect = function(data) {
    // set model
    model.setViewSelected(data);
    // update view
    view.updateMenu(viewSelect, resetSelect);
    view.updateReset(viewSelect, resetSelect);
    view.updateInfo(viewSelect);
    view.updateCredits(viewSelect);
    view.updateQuiz(viewSelect, answerSelect, navSelect);
    view.updateList(viewSelect, questionsSelect);
  }

  /**
   * QUIZ ANSWER
   */

  var answerSelect = function(element) {
    // set model
    model.setQuizAnswerSelected(element.attr("data-answer"));
    // update view
    view.updateQuiz(viewSelect, answerSelect, navSelect);
  }

  /**
   * QUIZ NAV
   */

  var navSelect = function(nav) {
    // set model
    if (nav == "prev") { model.setQuizPrevSelected(); }
    else if (nav == "shuf") { model.setQuizShuffleSelected(); }
    else if (nav == "next") { model.setQuizNextSelected(); }
    // update view
    viewSelect("quiz");
  }

  /**
   * LIST QUESTIONS
   */

  var questionsSelect = function(element) {
    console.log(element.attr("data-question"));
    // set model
    model.setQuizQuestionSelected(element.attr("data-question"));
    // update view
    viewSelect("quiz");
  }
}
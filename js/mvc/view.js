function RandomFactsView(model) {
  
  var self = this;

  /**
   * TEMPLATES
   */
  var templateMenu = Handlebars.compile($("#menu-template").html());
  var templateReset = Handlebars.compile($("#reset-template").html());
  var templateInfo = Handlebars.compile($("#info-template").html());
  var templateCredits = Handlebars.compile($("#credits-template").html());

  var templateQuiz = Handlebars.compile($("#quiz-template").html());
  var templateQuizImage = Handlebars.compile($("#quiz-image-template").html());
  var templateQuizAnswer = Handlebars.compile($("#quiz-answer-template").html());
  var templateQuizTwitter = Handlebars.compile($("#quiz-twitter-template").html());
  
  var templateList = Handlebars.compile($("#list-template").html());
  var templateListQuestion = Handlebars.compile($("#list-question-template").html());
  var templateListQuestionIcon = Handlebars.compile($("#list-question-icon-template").html());

  /**
   * RESIZE
   */
  
  var resizeTimeout;
  
  this.resize = function() {
    if (resizeTimeout) { clearTimeout(resizeTimeout); }
    resizeTimeout = setTimeout(self.resizeView, 200);
  }
  
  this.resizeView = function() {
    // get height
    var windowHeight = $(window).height();
    // fix for Safari iOS bug
    var windowHeight = (window.innerHeight) ? window.innerHeight : $(window).height();

    // get height - info
    var infoTopHeight = $(".info-top").outerHeight();
    var creditsTopHeight = $(".credits-top").outerHeight();
    // get height - quiz
    var quizTopHeight = $(".quiz-top").outerHeight();
    var quizBottomHeight = $(".quiz-bottom").outerHeight();
    // get height - list
    var listTopHeight = $(".list-top").outerHeight();
    var listBottomHeight = $(".list-bottom").outerHeight();
    // set height
    $(".menu-scroll").height(windowHeight);
    $(".reset-scroll").height(windowHeight);
    $(".info-scroll").height(windowHeight-infoTopHeight);
    $(".credits-scroll").height(windowHeight-creditsTopHeight);
    $(".quiz-scroll").height(windowHeight-quizTopHeight-quizBottomHeight);
    $(".list-scroll").height(windowHeight-listTopHeight-listBottomHeight);

    // get width - quiz
    var quizScrollWidth = $(".quiz-scroll").outerWidth();
    // set image - quiz
    $(".quiz-image img").height(Math.round(quizScrollWidth / 2));

    // remove style
    $(".menu-scroll").removeClass("scroll");
    $(".reset-scroll").removeClass("scroll");
    $(".info-scroll").removeClass("scroll");
    $(".credits-scroll").removeClass("scroll");
    $(".quiz-scroll").removeClass("scroll");
    $(".list-scroll").removeClass("scroll");

    // prevent swipe
    $(".menu-scroll").on("touchmove", function(e) { e.preventDefault(); });
    $(".reset-scroll").on("touchmove", function(e) { e.preventDefault(); });
    $(".info-scroll").on("touchmove", function(e) { e.preventDefault(); });
    $(".credits-scroll").on("touchmove", function(e) { e.preventDefault(); });
    $(".quiz-scroll").on("touchmove", function(e) { e.preventDefault(); });
    $(".list-scroll").on("touchmove", function(e) { e.preventDefault(); });

    // get height - menu
    var menuContentsHeight = $(".menu-content").outerHeight(true);
    // get height - reset
    var resetContentsHeight = $(".reset-content").outerHeight(true);
    // get height - info
    var infoScrollHeight = $(".info-scroll").outerHeight();
    var infoContentsHeight = $(".info-content").outerHeight(true);
    // get height - info
    var creditsScrollHeight = $(".credits-scroll").outerHeight();
    var creditsContentsHeight = $(".credits-content").outerHeight(true);
    // get height - quiz
    var quizScrollHeight = $(".quiz-scroll").outerHeight();
    var quizContentsHeight = $(".quiz-content").outerHeight(true);
    // get height - list
    var listScrollHeight = $(".list-scroll").outerHeight();
    var listContentsHeight = $(".list-questions").outerHeight(true);

    // add scroll
    if (menuContentsHeight > windowHeight) {
      $(".menu-scroll").addClass("scroll");
      $(".menu-scroll").off("touchmove");
      $(".menu-scroll").on("touchmove", function(e) { e.stopPropagation(); });
    }
    if (resetContentsHeight > windowHeight) {
      $(".reset-scroll").addClass("scroll");
      $(".reset-scroll").off("touchmove");
      $(".reset-scroll").on("touchmove", function(e) { e.stopPropagation(); });
    }
    if (infoContentsHeight > infoScrollHeight) {
      $(".info-scroll").addClass("scroll");
      $(".info-scroll").off("touchmove");
      $(".info-scroll").on("touchmove", function(e) { e.stopPropagation(); });
    }
    if (creditsContentsHeight > creditsScrollHeight) {
      $(".credits-scroll").addClass("scroll");
      $(".credits-scroll").off("touchmove");
      $(".credits-scroll").on("touchmove", function(e) { e.stopPropagation(); });
    }
    if (quizContentsHeight > quizScrollHeight) {
      $(".quiz-scroll").addClass("scroll");
      $(".quiz-scroll").off("touchmove");
      $(".quiz-scroll").on("touchmove", function(e) { e.stopPropagation(); });
    }
    if (listContentsHeight > listScrollHeight) {
      $(".list-scroll").addClass("scroll");
      $(".list-scroll").off("touchmove");
      $(".list-scroll").on("touchmove", function(e) { e.stopPropagation(); });
    }
  }

  /**
   * MENU
   */

  this.updateMenu = function(viewSelect, resetSelect) {
    // remove menu
    $("#menu").remove();

    // update menu
    if (model.getViewSelected() == "menu") {

      // create menu
      var menu = $("<div>", { id:"menu" });
      // append menu
      $("#app").append(menu);

      // set content
      var content = templateMenu();
      // append content
      $("#menu").append(content);

      // resize on image load
      $(".menu-mast img").bind('load', function() { self.resizeView(); });
      $(".menu-title img").bind('load', function() { self.resizeView(); });

      // set colour
      updateColour(model.getBackgroundColour());

      // set style
      if (model.getQuizContinue()) {
        $(".menu-continue").addClass("link");
        $(".menu-continue").removeClass("disabled");
      } else {
        $(".menu-continue").removeClass("link");
        $(".menu-continue").addClass("disabled");
      }

      // bind events
      $(".menu-info").on("click", function() { viewSelect("info"); });
      $(".menu-credits").on("click", function() { viewSelect("credits"); });

      // bind events
      if (model.getQuizContinue()) {
        $(".menu-new").on("click", function() { viewSelect("reset"); });
        $(".menu-continue").on("click", function() { viewSelect("quiz"); });
      } else {
        $(".menu-new").on("click", function() { resetSelect(); });
      }

      // update view
      self.resizeView();
    }
  }

  /**
   * RESET
   */

  this.updateReset = function(viewSelect, resetSelect) {
    // remove reset
    $("#reset").remove();

    // update reset
    if (model.getViewSelected() == "reset") {

      // create reset
      var reset = $("<div>", { id:"reset" });
      // append reset
      $("#app").append(reset);

      // set content
      var content = templateReset();
      // append content
      $("#reset").append(content);

      // resize on image load
      $(".reset-mast img").bind('load', function() { self.resizeView(); });
      $(".reset-title img").bind('load', function() { self.resizeView(); });

      // set colour
      updateColour(model.getBackgroundColour());

      // bind events
      $(".reset-yes").on("click", function() { resetSelect(); });
      $(".reset-no").on("click", function() { viewSelect("menu"); });

      // update view
      self.resizeView();
    }
  }

  /**
   * INFO
   */

  this.updateInfo = function(viewSelect) {
    // remove info
    $("#info").remove();

    // update info
    if (model.getViewSelected() == "info") {

      // create info
      var info = $("<div>", { id:"info" });
      // append info
      $("#app").append(info);

      // set html
      var html = "";
      // get extra text
      var extraData = model.getExtraData();
      for (var i in extraData) {
        if (extraData[i]["section"] == "how to play") {
          html = extraData[i]["html"];
        }
      }

      // set content
      var content = templateInfo({ html: html });
      // append content
      $("#info").append(content);

      // set colour
      updateColour(model.getBackgroundColour());

      // bind events
      $(".info-menu").on("click", function() { viewSelect("menu"); });
      $(".info-quiz").on("click", function() { viewSelect("quiz"); });

      // remove inactive controls
      if (!model.getQuizContinue()) {
        $(".info-quiz").off("click");
        $(".info-quiz").addClass("deactive");
      }

      // update view
      self.resizeView();
    }
  }

  /**
   * CREDITS
   */

  this.updateCredits = function(viewSelect) {
    // remove credits
    $("#credits").remove();

    // update credits
    if (model.getViewSelected() == "credits") {

      // create credits
      var credits = $("<div>", { id:"credits" });
      // append credits
      $("#app").append(credits);

      // set html
      var html = "";
      // get extra text
      var extraData = model.getExtraData();
      for (var i in extraData) {
        if (extraData[i]["section"] == "credits") {
          html = extraData[i]["html"];
        }
      }

      // set content
      var content = templateCredits({ html: html });
      // append content
      $("#credits").append(content);

      // set colour
      updateColour(model.getBackgroundColour());

      // bind events
      $(".credits-menu").on("click", function() { viewSelect("menu"); });
      $(".credits-quiz").on("click", function() { viewSelect("quiz"); });

      // remove inactive controls
      if (!model.getQuizContinue()) {
        $(".credits-quiz").off("click");
        $(".credits-quiz").addClass("deactive");
      }

      // update view
      self.resizeView();
    }
  }

  /**
   * QUIZ
   */

  this.updateQuiz = function(viewSelect, answerSelect, navSelect) {
    // remove element
    $("#quiz").remove();

    // update quiz
    if (model.getViewSelected() == "quiz") {
      // create element
      $("#app").append($("<div>", { id:"quiz" }));

      // get answer
      var answer = model.getQuizAnswerSelected();

      // set number
      var number = model.getQuizIndexSelected() + 1;

      // set image
      var image = "";
      var imageSrc = model.getQuizImageSelected();
      if (imageSrc) { image = templateQuizImage({ image: imageSrc }); }

      // set question
      var question = model.getQuizQuestionSelected();

      // set answers
      var answers = "";
      // get answers
      var answerArray = model.getQuizAnswersSelected();
      // loop through answers
      for (var i in answerArray) {
        answers += templateQuizAnswer({
          index: i,
          style: model.getAnswerStyle(parseInt(i)),
          letter: model.getAnswerLetter(parseInt(i)),
          answer: answerArray[i].answer
        });
      }

      // set values
      var correct = "";
      var fact = "";
      var tweet = "";
      var twitter = "";
      // if answered
      if (answer != null) {
        // set values
        correct = model.getQuizCorrectSelected();
        fact = model.getQuizFactSelected();
        tweet = model.getQuizTweetSelected();
        twitter = templateQuizTwitter({});
      }

      // set quiz
      var quiz = templateQuiz({
        number: number,
        image: image,
        question: question,
        answers: answers,
        correct: correct,
        fact: fact,
        twitter: twitter
      });

      // append quiz
      $("#quiz").append(quiz);

      // set colour
      updateColour(model.getBackgroundColour());

      // bind events
      $(".quiz-menu").on("click", function() { viewSelect("menu"); });
      $(".quiz-info").on("click", function() { viewSelect("info"); });
      $(".quiz-prev").on("click", function() { navSelect("prev"); });
      $(".quiz-list").on("click", function() { viewSelect("list"); });
      $(".quiz-shuf").on("click", function() { navSelect("shuf"); });
      $(".quiz-next").on("click", function() { navSelect("next"); });

      // bind events - not answered
      if (answer == null) {
        $(".quiz-answer").on("click", function() {
          // set scoll position
          model.setScrollPosition(number, $(".quiz-scroll").scrollTop());
          // call controller
          answerSelect($(this));
        });
      // bind events - answered
      } else {
        $(".quiz-twitter-text").on("click", function() {
          var twitter = "FACT: "+tweet+" http://goo.gl/JVCfUL - via @ST_Sport";
          window.open("https://twitter.com/intent/tweet?text="+twitter, "popupWindow", "width=600,height=500,scrollbars=yes");
        });
      }

      // remove inactive controls
      if (!model.getQuizPrevActive()) {
        $(".quiz-prev").off("click");
        $(".quiz-prev").addClass("deactive");
      }
      if (!model.getQuizNextActive()) {
        //$(".quiz-next").off("click");
        //$(".quiz-next").addClass("deactive");
        $(".quiz-next").on("click", function() { viewSelect("list"); });
      }

      // update view
      self.resizeView();

      // answered - scroll to
      if (answer != null) {
        // get scoll positions
        var correctPos = $(".quiz-correct").offset().top;
        var previousPos = model.getScrollPosition(number);

        if (previousPos != null) {
          // set scroll position
          updateQuizScroll(previousPos, 0);
          // set scroll position via timeout
          setTimeout(function() { updateQuizScroll(correctPos, 400); }, 50);
        } else {
          // set scroll position
          updateQuizScroll(correctPos, 0);
        }

        // clear scoll position
        model.setScrollPosition(0, 0);
      }
    }
  }

  var updateQuizScroll = function(scrollPos, scrollSpeed) {
    // set scroll position
    $(".quiz-scroll").animate({ scrollTop: scrollPos }, scrollSpeed);
  }

  /**
   * LIST
   */

  this.updateList = function(viewSelect, questionsSelect) {
    // remove element
    $("#list").remove();

    // update list
    if (model.getViewSelected() == "list") {
      // create element
      $("#app").append($("<div>", { id:"list" }));

      // set questions
      var questions = "";
      
      // get questions
      var questionArray = model.getQuizQuestions();
      // loop through questions
      for (var i in questionArray) {
        // set values
        var selected = false;
        var correct = false;
        // loop through questions
        for (var ii in questionArray[i].answers) {
          if (questionArray[i].answers[ii].selected == true) { selected = true;
            if (questionArray[i].answers[ii].correct == true) { correct = true; }
          }
        }

        // set icon
        var icon = "";
        if (selected) {
          var image = (correct) ? "correct" : "wrong";
          icon = templateListQuestionIcon({ image: image });
        }
        
        // set questions
        questions += templateListQuestion({
          index: parseInt(i),
          number: parseInt(i) + 1,
          answer: questionArray[i].question,
          icon: icon
        });
      }

      // set list
      var list = templateList({
        questions: questions,
        score: model.getScoreText(),
        complete: model.getCompleteText()
      });

      // append list
      $("#list").append(list);

      // set colour
      updateColour(model.getBackgroundColour());

      // bind events
      $(".list-menu").on("click", function() { viewSelect("menu"); });
      $(".list-quiz").on("click", function() { viewSelect("quiz"); });

      // bind events - not answered
      $(".list-question").on("click", function() { questionsSelect($(this)); });

      // update view
      self.resizeView();
    }
  }

  /**
   * COLOUR
   */

  var updateColour = function(colour) {
    // set background colour
    $("#app-container").css({"background": "#"+colour});
    // get gradient rgb
    var rgb = model.hexToRgb(colour);
    var code = rgb.r+","+rgb.g+","+rgb.b;
    // get view
    var view = model.getViewSelected();
    // set gradient
    $("."+view+"-gradient-top").css({"background": "-webkit-linear-gradient(bottom, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
    $("."+view+"-gradient-top").css({"background": "-moz-linear-gradient(bottom, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
    $("."+view+"-gradient-top").css({"background": "-o-linear-gradient(bottom, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
    $("."+view+"-gradient-top").css({"background": "-ms-linear-gradient(bottom, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
    $("."+view+"-gradient-top").css({"background": "linear-gradient(to top, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
    // set gradient
    $("."+view+"-gradient-bottom").css({"background": "-webkit-linear-gradient(top, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
    $("."+view+"-gradient-bottom").css({"background": "-moz-linear-gradient(top, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
    $("."+view+"-gradient-bottom").css({"background": "-o-linear-gradient(top, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
    $("."+view+"-gradient-bottom").css({"background": "-ms-linear-gradient(top, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
    $("."+view+"-gradient-bottom").css({"background": "linear-gradient(to bottom, rgba("+code+",0) 0%,rgba("+code+",1) 100%)"});
  }
}
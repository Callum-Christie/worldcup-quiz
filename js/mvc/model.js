function RandomFactsModel() {

  var self = this;

  /**
   * VIEW
   */

  var viewData;

  this.setViewSelected = function(view) { viewData = "menu";
    if (view == "reset" || view == "info" || view == "credits" || view == "quiz" || view == "list") {
      viewData = view;
    }
  }

  this.getViewSelected = function() {
    return viewData;
  }

  /**
   * QUIZ
   */

  var quizData;
  var extraData;

  this.resetQuizData = function() {
    // reset selected
    for (var i in quizData) {
      quizData[i].selected = false;
      for (var ii in quizData[i].answers) {
        quizData[i].answers[ii].selected = false;
      }
    }
    // set default question selected
    self.setQuizSelected(0);
  }

  this.setQuizLocalData = function(data) {
    // set data
    quizData = data;
    // set default question selected
    self.setQuizSelected(0);
  }

  this.setGoogleQuestions = function(data) {
    quizData = [];
    // parse google spreadsheet
    for (var i in data.feed.entry) {
      // set answer data
      var answerData = [];
      // set answer values
      var answers = [
        { "answer": data.feed.entry[i].gsx$correct.$t, "origin": 0, "correct": true },
        { "answer": data.feed.entry[i].gsx$wronga1.$t, "origin": 1 },
        { "answer": data.feed.entry[i].gsx$wronga2.$t, "origin": 2 },
        { "answer": data.feed.entry[i].gsx$wronga3.$t, "origin": 3 }];
      // randomise answer data
      for (var ii = 0; ii < 4; ii++) {
        var random = Math.floor(Math.random() * answers.length);
        answerData.push(answers.splice(random, 1)[0]);
      }
      // set quiz data
      quizData.push({
        "image": data.feed.entry[i].gsx$image.$t,
        "question": data.feed.entry[i].gsx$question.$t,
        "answers": answerData,
        "fact": data.feed.entry[i].gsx$fact.$t,
        "tweet": data.feed.entry[i].gsx$tweet.$t
      });
    }
    // set default question selected
    self.setQuizSelected(0);
  }

  this.setGoogleExtraText = function(data) {
    extraData = [];
    // parse google spreadsheet
    for (var i in data.feed.entry) {
      // set extra data
      extraData.push({
        "section": data.feed.entry[i].gsx$section.$t,
        "html": data.feed.entry[i].gsx$html.$t
      });
    }
  }

  this.getQuizQuestions = function() {
    return quizData;
  }

  this.getExtraData = function() {
    return extraData;
  }

  this.setQuizSelected = function(index) {
    for (var i in quizData) {
      // unselect question
      quizData[i].selected = false;
      // set question selected
      if (i == index) { quizData[i].selected = true; }
    }
  }

  this.getQuizPrevActive = function() {
    // get index selected
    var index = self.getQuizIndexSelected();
    // set question selected
    if (index > 0) { return true; }
  }

  this.setQuizPrevSelected = function() {
    // get index selected
    var index = self.getQuizIndexSelected();
    // set question selected
    if (index > 0) { self.setQuizSelected(index - 1); }
  }

  this.setQuizShuffleSelected = function() {
    // select facts
    if (self.getQuizComplete()) {
      // set random number
      var random = Math.floor(Math.random() * (quizData.length - 1));
      // set count
      var count = 0;
      // set selected
      for (var i in quizData) {
        // exclude question selected
        if (quizData[i].selected != true) {
          if (count == random) { self.setQuizSelected(i); } count++;
        }
      }
    // select unanswered questions
    } else {
      // get answer selected
      var answerSelected = self.getQuizAnswerSelected();
      // set number of incomplete questions
      var incomplete = quizData.length - scoreComplete;
      // exclude question selected
      if (answerSelected == null) { incomplete--; }
      // set random number
      var random = Math.floor(Math.random() * incomplete);
      // set count
      var count = 0;
      // set selected
      for (var i in quizData) {
        // set complete flag
        var complete = false;
        for (var ii in quizData[i].answers) {
          // exclude question selected
          if (quizData[i].selected == true && answerSelected == null) { complete = true; }
          // exclude answer selected
          else if (quizData[i].answers[ii].selected == true) { complete = true; }
        }
        // set question selected
        if (complete == false) {
          if (count == random) { self.setQuizSelected(i); } count++;
        }
      }
    }
  }

  this.getQuizNextActive = function() {
    // get index selected
    var index = self.getQuizIndexSelected();
    // set question selected
    if (index < quizData.length - 1) { return true; }
  }

  this.setQuizNextSelected = function() {
    // get index selected
    var index = self.getQuizIndexSelected();
    // set question selected
    if (index < quizData.length - 1) { self.setQuizSelected(index + 1); }
  }

  this.setQuizQuestionSelected = function(index) {
    // set question selected
    if (index >= 0 && index <= quizData.length - 1) { self.setQuizSelected(index); }
  }

  this.getQuizIndexSelected = function() {
    for (var i in quizData) {
      if (quizData[i].selected == true) { return parseInt(i); }
    }
  }

  this.getQuizImageSelected = function() {
    return quizData[self.getQuizIndexSelected()].image;
  }

  this.getQuizQuestionSelected = function() {
    return quizData[self.getQuizIndexSelected()].question;
  }

  this.getQuizAnswersSelected = function() {
    return quizData[self.getQuizIndexSelected()].answers;
  }

  this.getAnswerLetter = function(index) {
    switch(index) {
      case 0: return "A";
      case 1: return "B";
      case 2: return "C";
      case 3: return "D";
    }
  }

  this.getAnswerStyle = function(index) {
    var i = self.getQuizIndexSelected();
    // not answered
    if (self.getQuizAnswerSelected() == null) { return "link"; }
    // answered
    if (quizData[i].answers[index].correct) { return "correct"; }
    else if (quizData[i].answers[index].selected) { return "incorrect"; }
  }

  this.getQuizCorrectSelected = function() {
    var i = self.getQuizIndexSelected();
    for (var ii in quizData[i].answers) {
      if (quizData[i].answers[ii].correct == true) { return parseInt(ii); }
    }
  }

  this.setQuizAnswerSelected = function(index) {
    quizData[self.getQuizIndexSelected()].answers[index].selected = true;
    // update score
    self.setScore();
    // update cookie
    self.setCookie();
  }

  this.getQuizAnswerSelected = function() {
    var i = self.getQuizIndexSelected();
    for (var ii in quizData[i].answers) {
      if (quizData[i].answers[ii].selected == true) { return parseInt(ii); }
    }
  }

  this.getQuizCorrectSelected = function() {
    var i = self.getQuizIndexSelected();
    for (var ii in quizData[i].answers) {
      if (quizData[i].answers[ii].correct == true) {
        if (quizData[i].answers[ii].selected == true) { return "Correct!"; }
      }
    }
    return "Wrong!";
  }

  this.getQuizFactSelected = function() {
    return quizData[self.getQuizIndexSelected()].fact;
  }

  this.getQuizTweetSelected = function() {
    return quizData[self.getQuizIndexSelected()].tweet;
  }

  this.getQuizContinue = function() {
    return (scoreComplete > 0) ? true : false;
  }

  this.getQuizComplete = function() {
    return (scoreComplete == quizData.length) ? true : false;
  }

  /**
   * SCORE
   */

  var scoreCorrect = 0;
  var scoreComplete = 0;

  this.setScore = function() {
    var anwersCorrect = 0;
    var anwersComplete = 0;
    // increment score
    for (var i in quizData) {
      for (var ii in quizData[i].answers) {
        if (quizData[i].answers[ii].selected == true) { anwersComplete++;
          if (quizData[i].answers[ii].correct == true) { anwersCorrect++; }
        }
      }
    }
    // set score
    scoreCorrect = anwersCorrect;
    scoreComplete = anwersComplete;
  }

  this.getScoreText = function() {
    return scoreCorrect+"/"+scoreComplete;
  }
  
  this.getCompleteText = function() {
    return Math.round((scoreComplete / quizData.length) * 100);
  }

  /**
   * COOKIE
   */

  var cookieName = "STWorldCupFacts";

  this.getCookie = function() {
    if ($.cookie(cookieName)) {
      // get cookie
      var cookie = $.cookie(cookieName).split(",");
      for (var i in cookie) {
        // get cookie item
        var item = cookie[i].split(":");
        if (item.length > 1) {
          // get question
          var question = parseInt(item[0]);
          if (question < quizData.length) {
            // get answer
            var answer = parseInt(item[1]);
            if (quizData[question].answers[0].origin != null) {
              for (var ii in quizData[question].answers) {
                if (quizData[question].answers[ii].origin == answer) { answer = ii; break; }
              }
            }
            // set answer selected
            quizData[question].answers[answer].selected = true;
          }
        }
      }
      // update selected
      for (var i in quizData) {
        // get selected
        var selected = false;
        for (var ii in quizData[i].answers) {
          if (quizData[i].answers[ii].selected == true) { selected = true; }
        }
        // set first unselected
        if (!selected) { self.setQuizSelected(i); break; }
      }
    }
    // update score
    self.setScore();
  }

  this.setCookie = function() {
    $.removeCookie(cookieName);
    // set cookie data
    var cookieData = "";
    // get answers
    for (var i in quizData) {
      for (var ii in quizData[i].answers) {
        if (quizData[i].answers[ii].selected == true) {
          // set answers
          var answer = parseInt(ii);
          if (quizData[i].answers[ii].origin != null) {
            answer = quizData[i].answers[ii].origin;
          }
          // set cookie data
          cookieData += i+":"+answer+",";
        }
      }
    }
    // set cookie
    $.cookie(cookieName, cookieData);
  }

  this.removeCookie = function() {
    $.removeCookie(cookieName);
  }

  /**
   * SCROLL
   */

  var scrollQuestion;
  var scrollPos;

  this.setScrollPosition = function(question, pos) {
    scrollQuestion = question;
    scrollPos = pos;
  }

  this.getScrollPosition = function(question) {
    if (scrollQuestion == question) { return scrollPos; }
  }

  /**
   * BACKGROUND
   */

  this.getBackgroundColour = function() {
    // set colours
    //var colours = ["59b54a", "bfd730", "ffc805", "c40075"];
    var colours = ["59b54a", "0198E1", "c40075"];
    // get steps
    var steps = quizData.length;
    var step = self.getQuizIndexSelected();
    // set colour steps - number of steps between colours
    var colourSteps = steps / (colours.length - 1);
    // set colour index - first colour to use
    var colourIndex = Math.floor(step / colourSteps);
    // set colour step - current position between colours
    var colourStep = Math.floor(step % colourSteps);
    // normailse colour interval
    colourSteps = Math.ceil(colourSteps);
    // return colour hex code
    return self.getColourTween(colours[colourIndex], colours[colourIndex + 1], colourSteps, colourStep);
  }

  this.getColourTween = function(colour1, colour2, steps, step) {
    // covert hex to decimal
    var colour1Red = parseInt(colour1.substr(0, 2), 16);
    var colour1Green = parseInt(colour1.substr(2, 2), 16);
    var colour1Blue = parseInt(colour1.substr(4, 2), 16);
    var colour2Red = parseInt(colour2.substr(0, 2), 16);
    var colour2Green = parseInt(colour2.substr(2, 2), 16);
    var colour2Blue = parseInt(colour2.substr(4, 2), 16);
    // create colour difference
    var incrementRed = (colour2Red - colour1Red) / steps;
    var incrementGreen = (colour2Green - colour1Green) / steps;
    var incrementBlue = (colour2Blue - colour1Blue) / steps;
    // create new colour step value
    var stepRed = Math.round(colour1Red + (incrementRed * step));
    var stepGreen = Math.round(colour1Green + (incrementGreen * step));
    var stepBlue = Math.round(colour1Blue + (incrementBlue * step));
    // covert decimal to hex
    var colourRed = stepRed.toString(16);
    var colourGreen = stepGreen.toString(16);
    var colourBlue = stepBlue.toString(16);
    // add leading zeros
    colourRed = (colourRed.length == 1) ? "0"+colourRed : colourRed;
    colourGreen = (colourGreen.length == 1) ? "0"+colourGreen : colourGreen;
    colourBlue = (colourBlue.length == 1) ? "0"+colourBlue : colourBlue;
    // return colour hex code
    return colourRed+colourGreen+colourBlue;
  }

  this.hexToRgb = function(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }
}